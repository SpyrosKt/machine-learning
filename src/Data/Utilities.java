
package Data;

import java.util.*;

public class Utilities {
	
	public static final double LOG_OF_TWO = Math.log(2);
	private static final int DEFAULT_LENGTH = 12;
	
	private static Map<DataTable, Double> classEntropies = new HashMap<>();		// class entropies of DataTables will be stored here
	
	
	public static double calculateEntropy(DataTable dataTable, int columnIndex) {
		// maps each existing value of the attribute to an occurrence counter
		Map<Object, Integer> occurrences = dataTable.getOccurrences(columnIndex);
		Object currentElement;
		
		double entropy = 0;
		double currentProbability;
		for(Object value : occurrences.keySet()) {
			currentProbability = (double) occurrences.get(value) / dataTable.rowCount();
			entropy -= currentProbability * log2(currentProbability);
		}
		return entropy;
	}
	
	public static double calculateClassEntropy(DataTable dataTable) {
		return calculateEntropy(dataTable, dataTable.getClassIndex());
	}
	
	@Deprecated
	public static double calculateAndStoreClassEntropy(DataTable dataTable) {
		// if dataTable is already mapped to a class entropy, no need to calculate it again - UNLESS its class column has been altered since the last calculation
		if(!classEntropies.containsKey(dataTable) || dataTable.isClassColumnAltered()) {
			classEntropies.put(dataTable, calculateEntropy(dataTable, dataTable.getClassIndex()));
			dataTable.setClassColumnAltered(false);
		}
		
		return classEntropies.get(dataTable);
	}
	
	public static Object mostCommonValue(DataTable dataTable, DataColumn column) {
		int max = -1;
		Object mostCommonValue = null;
		
		int currentValueOccurrences;
		for(Object value : dataTable.getOccurrences(column).keySet()) {
			currentValueOccurrences = dataTable.getOccurrences(column).get(value);
			if(currentValueOccurrences > max) {
				max = currentValueOccurrences;
				mostCommonValue = value;
			}
		}
		
		return mostCommonValue;
	}
	
	public static Object mostCommonClassValue(DataTable dataTable) {
		return mostCommonValue(dataTable, dataTable.columns.get(dataTable.classIndex));
	}
	
	/**
	 * Calculates the Information Gain of the attribute at the given index
	 * The dataset gets split to parts - one for each value of the attribute
	 * Then, each part's class' entropy gets multiplied by the amount of the part's entries over the amount of initial entries
	 * (which is the probability of the part's attribute value appearing in the initial dataset)
	 * Each of the aforementioned products gets subtracted from the initial dataset's class' entropy
	 * The type is (where X is the attribute, x any of its values and C the class):
	 * IG(X, C) = H(C) - Σx[P(X=x)*H(C|X=x)]
	 * @param index The index of the attribute whose information gain will be calculated
	 */
	public static double calculateIG(DataTable dataTable, int index) {
		// IG(X, C) = H(C) - Σc[P(C=c)*H(C|C=c)] = H(C), since H(C|C=c) = 0 (for any c)
		
		if(index == dataTable.getClassIndex())
			return calculateClassEntropy(dataTable);
		
		Map<Object, DataTable> subTables = dataTable.splitAt(index);
		double ig = calculateClassEntropy(dataTable);
		double currentProbability;
		for(DataTable d : subTables.values()) {
			currentProbability = (double) d.rowCount() / dataTable.rowCount();
			ig -=  currentProbability * calculateClassEntropy(d);
		}
		return ig;
	}
	
	public static int indexOfHighestIGAttribute(DataTable dataTable) {
		int indexOfMax = -1;
		double maxIG = -1.0;
		double currentIG;
		for(int i = 0; i < dataTable.columnCount(); i++) {
			if(i == dataTable.getClassIndex())
				continue;
			currentIG = calculateIG(dataTable, i);
			if(currentIG > maxIG) {
				indexOfMax = i;
				maxIG = currentIG;
			}
		}
		return  indexOfMax;
	}
	
	public static DataColumn highestIGAttribute(DataTable dataTable) {
		int index = indexOfHighestIGAttribute(dataTable);
		return dataTable.getColumn(index);
	}
	
	public static String nameOfHighestIGAttribute(DataTable dataTable) {
		int index = indexOfHighestIGAttribute(dataTable);
		return dataTable.getColumn(index).getName();
	}
	
	public static double log2(double x) {
		return (Math.log(x) / LOG_OF_TWO);		// Logb(x) = Loga(x)/Loga(b)
	}
	
	public static void printInfo(DataTable dataTable) {
		System.out.println("Class (index): " + dataTable.getClassName() + " (" + dataTable.getClassIndex() + ")");
		dataTable.print();
		System.out.println();
		
		System.out.println("Entropy: ");
		for(int i = 0; i < dataTable.columnCount(); i++)
			System.out.println(dataTable.getColumnName(i) + ": " + calculateEntropy(dataTable, i));
		System.out.println();
		
		System.out.println("IG: ");
		for(int i = 0; i < dataTable.columnCount(); i++)
			System.out.println(dataTable.getColumnName(i) + ": " + calculateIG(dataTable, i));
		System.out.println();
		
		System.out.println("Possible values (occurrences): ");
		for(int i = 0; i < dataTable.columnCount(); i++) {
			System.out.print(dataTable.getColumnName(i) + ":\t[");
			DataColumn currentColumn = dataTable.getColumn(i);
			
			Set<Object> possibleColumnValues = dataTable.getPossibleValues().get(currentColumn);
			int counter = 0;
			for(Object possibleValue : possibleColumnValues) {
				System.out.print(possibleValue + " (" + dataTable.getOccurrences(currentColumn, possibleValue) + ")");
				counter++;
				if(counter < possibleColumnValues.size())
					System.out.print(", ");
			}
			System.out.println("]");
		}
		System.out.println();
		
		System.out.println("Existing values (occurrences): ");
		for(int i = 0; i < dataTable.columnCount(); i++) {
			System.out.print(dataTable.getColumnName(i) + ":\t[");
			DataColumn currentColumn = dataTable.getColumn(i);
			
			Set<Object> existingColumnValues = dataTable.getExistingValues().get(currentColumn);
			int counter = 0;
			for(Object existingValue : existingColumnValues) {
				System.out.print(existingValue + " (" + dataTable.getOccurrences(currentColumn, existingValue) + ")");
				counter++;
				if(counter < existingColumnValues.size())
					System.out.print(", ");
			}
			System.out.println("]");
		}
		System.out.println();
	}
	
	private String fixWidth(String s) {
		return fixWidth(s, DEFAULT_LENGTH);
	}
	
	private String fixWidth(String s, int desirableLength) {
		if(s.length() > desirableLength)
			return (s.substring(0, desirableLength - 1) + ".");
		// add spaces while attempting to keep the initial string centered
		for(int i = s.length(); i < desirableLength; i++)
			if(i % 2 == 0)
				s = s.concat(" ");
			else
				s = " ".concat(s);
		return s;
	}
	
}
