
package Data;

import java.io.*;
import java.util.*;

public class DataTable {
	
	protected List<DataRow> rows = new ArrayList<>();		// for simplicity's sake, all attributes-columnValues are treated as Strings
	protected List<String> columnNames = new ArrayList<>();
	protected List<DataColumn> columns = new ArrayList<>();
	
	protected int classIndex = -1;		// indicates which column represents the "class" attribute
	protected String className;			// this is "calculatable" but comes up too often
	protected boolean classColumnAltered = false;
	
	protected Map<DataColumn, Set<Object>> possibleValues = new HashMap<>();		// an attempt to keep an eye on the all the possible values each column can have
	protected Map<DataColumn, Set<Object>> existingValues = new HashMap<>();		// keeps track of all the values that actually exist in a column
	protected Map<DataColumn, Map<Object, Integer>> existingValueOccurrences = new HashMap<>();	// same as above, but keeps count of the occurrences of each value
	protected Map<DataColumn, List<Double>> possibleColumnBounds = new HashMap<>();		// keeps track of the maximum and minimum values that are possible in a column - only for numeric columns
	
	
	public DataTable() {}
	
	public DataTable(String filePath) throws IOException {
		boolean classFound = false;
		
		BufferedReader br = new BufferedReader(new FileReader(filePath));
		
		String currentEntry, currentElement;
		StringTokenizer tokenizedEntry;
		
		// the file's first line contains the attributes' names separated by commas - supposedly
		if((currentEntry = br.readLine()) != null) {
			// delimeters do NOT contain whitespace character since they can appear in an attribute name - whitespace will be trimmed later on
			tokenizedEntry = new StringTokenizer(currentEntry, ",");
			
			int tokenIndex = 0;
			DataColumn currentColumn;
			
			// fill columnNames and columns
			while(tokenizedEntry.hasMoreTokens()) {
				currentElement = tokenizedEntry.nextToken();
				currentElement = currentElement.trim();
				
				if(DataColumn.columnExists(currentElement))
					currentColumn = DataColumn.getColumn(currentElement);
				else
					currentColumn = new DataColumn(currentElement, DataColumn.UNKNOWN_TYPE);
				
				this.addColumn(currentColumn);		// CAUTION: It is possible that the column type has not yet been decided
				
				currentElement = currentElement.replaceAll("[\"\']", "");
				
				// consider as class attribute the one with a name equal to "Class" or "Category"
				if(!classFound && (currentElement.equalsIgnoreCase("class") || currentElement.equalsIgnoreCase("category"))) {
					this.classIndex = tokenIndex;
					classFound = true;
				}
				tokenIndex++;
			}
			
		}
		
		if(!classFound)
			this.classIndex = columns.size() - 1;
		this.className = columns.get(classIndex).getName();
		
		
		boolean readingFirstRow = true;		// currently reading the first actual row
		
		// read the rest of the lines, which should contain the entries
		while((currentEntry = br.readLine()) != null) {
			
			// attribute columnValues are expected to be separated with a comma
			// delimeters do NOT contain whitespace character since they can appear in an attribute value - whitespace will be trimmed later on
			tokenizedEntry = new StringTokenizer(currentEntry, ",");
			
			DataRow dataRowToAdd = new DataRow();
			for(DataColumn column : columns)
				dataRowToAdd.addColumn(column);
			
			// iterate the current entry's attributes
			int tokenIndex = 0;
			while(tokenizedEntry.hasMoreTokens()) {
				currentElement = tokenizedEntry.nextToken();
				currentElement = currentElement.trim();
				
				String currentColumnName = columnNames.get(tokenIndex);
				DataColumn currentColumn = this.columns.get(tokenIndex);
				
				Object castedElement;
				
				// when reading the first row, decide the column types
				if(readingFirstRow) {
					try {
						castedElement = Double.parseDouble(currentElement);
						setColumnType(currentColumn, Double.class);
					}
					catch(Exception e) {
						castedElement = currentElement;
						setColumnType(currentColumn, String.class);
					}
				}
				// when reading the following lines, use the already "decided" types
				else {
					if(currentColumn.getType() == Double.class)
						castedElement = Double.parseDouble(currentElement);
					else
						castedElement = currentElement;
				}
				
				dataRowToAdd.set(currentColumnName, castedElement);		// columns need to be added to the row before this
				registerPossibleValueOccurrence(currentColumn, castedElement);
				
				tokenIndex++;
			}
			
			if(readingFirstRow)
				readingFirstRow = false;
			
			this.rows.add(dataRowToAdd);
		}
		
	}
	
	public DataTable(String filepath, String className) throws IOException {
		this(filepath);
		this.classIndex = this.columnNames.indexOf(className);
	}
	
	/**
	 * Returns a dataset consisting of COPIES of the source's entries on which the given attribute (index) has the given value
	 * The new dataset has the source dataset's existingValues and possibleValues (although not all possibleValues may appear in the new one)
	 * Furthermore, the "critical" attribute gets removed from the newly created dataset's columns, columnNames, possibleValues and existingValues; the classIndex also gets adjusted
	 * @param source			The original dataset from which the rows will be copied
	 * @param columnIndex		The index of the "critical" attribute - the attribute based on which the new dataset rows will be distinguished
	 * @param columnValue		The value on which the source dataset's entry will be copied to the new dataset
	 */
	public DataTable(DataTable source, int columnIndex, Object columnValue) {
		// copy the columns
		this.columnNames = new ArrayList<>(source.columnNames);
		this.columns = new ArrayList<>(source.columns);
		
		// keep the column info
		DataColumn column = columns.get(columnIndex);
		String columnName = columnNames.get(columnIndex);
		boolean columnIsNumeric = (column.getType() == Double.class);
		
		// remove the appropriate column from the new DataTable
		this.columnNames.remove(columnIndex);
		this.columns.remove(columnIndex);
		
		this.possibleValues = new HashMap<>(source.possibleValues);
		this.possibleValues.remove(column);
		
		this.existingValues = new HashMap<>(source.existingValues);
		this.existingValues.remove(column);
		
		this.existingValueOccurrences = new HashMap<>(source.existingValueOccurrences);
		this.existingValueOccurrences.remove(column);
		
		// the case when columnIndex = initial.classIndex is also handled, although splitting by class shouldn't be a thing
		this.classIndex = (columnIndex <= source.classIndex) ? source.classIndex - 1 : source.classIndex;
		
		for(DataRow row : source.rows) {
			boolean valuesMatch;
			
			// if the column is numeric, values need to be casted before comparing
			if(columnIsNumeric) {
				Double d;
				try {
					d = Double.parseDouble(columnValue.toString());		// this is necessary in case columnValue is Integer, Float, etc.
					valuesMatch = row.get(columnName).equals(d);
				}
				catch(Exception e) {
					valuesMatch = false;
				}
			}
			else {
				valuesMatch = row.get(columnName).equals(columnValue);
			}
			
			if(valuesMatch) {
				this.rows.add(new DataRow(row));
				this.getLastRow().removeColumn(columnName);
			}
		}
		
	}
	
	public DataTable(DataTable source, String columnName, Object columnValue) {
		this(source, source.columnNames.indexOf(columnName), columnValue);
	}
	
	public DataTable(DataTable source) {
		for(DataRow sourceDataRow : source.rows)
			this.rows.add(new DataRow(sourceDataRow));
		
		this.columnNames = new ArrayList<>(source.columnNames);
		this.columns = new ArrayList<>(source.columns);
		this.possibleValues = new HashMap<>(source.possibleValues);
		this.existingValues = new HashMap<>(source.existingValues);
		this.existingValueOccurrences = new HashMap<>(source.existingValueOccurrences);
		this.possibleColumnBounds = new HashMap<>(possibleColumnBounds);
		this.classIndex = source.classIndex;
		this.className = source.className;
		this.classColumnAltered = source.classColumnAltered;
	}
	
	public List<DataRow> getRows() {
		return rows;
	}
	
	public DataRow getRow(int index) {
		return rows.get(index);
	}
	
	protected DataRow getLastRow() {
		return this.rows.get(rowCount() - 1);
	}
	
	public List<DataColumn> getColumns() {
		return columns;
	}
	
	public DataColumn getColumn(int index) {
		return columns.get(index);
	}
	
	protected String getColumnName(int index) {return columnNames.get(index);}
	
	private void setColumnType(DataColumn column, Class type) {
		column.setType(type);
		
		// if the column's new type is a numeric type, new possible bounds need to be created for it - if they don't already exist (ie the old type was numeric too)
		if(column.isNumeric() && !possibleColumnBounds.containsKey(column))
			this.possibleColumnBounds.put(column, new ArrayList<>());
	}
	
	public DataRow removeRow(int rowIndex) {
		DataRow removedRow = this.rows.remove(rowIndex);
		
		for(DataColumn column : this.columns)
			this.decreaseOccurrences(column, removedRow.get(column));
		
		return removedRow;
	}
	
	public Map<DataColumn, Set<Object>> getExistingValues() {
		return this.existingValues;
	}
	
	public Map<DataColumn, Set<Object>> getPossibleValues() {
		return this.possibleValues;
	}
	
	public Set<Object> getPossibleValues(int columnIndex) {
		return this.possibleValues.get(this.columns.get(columnIndex));
	}
	
	public Set<Object> getPossibleValues(DataColumn column) {
		return this.possibleValues.get(column);
	}
	
	public List<Double> getPossibleColumnBounds(int columnIndex) {
		return this.possibleColumnBounds.get(this.columns.get(columnIndex));
	}
	
	public List<Double> getPossibleColumnBounds(DataColumn column) {
		return this.possibleColumnBounds.get(column);
	}
	
	public double getColumnRange(int columnIndex) {
		return getPossibleColumnBounds(columnIndex).get(1) - getPossibleColumnBounds(columnIndex).get(0);
	}
	
	public double getColumnRange(DataColumn column) {
		return getPossibleColumnBounds(column).get(1) - getPossibleColumnBounds(column).get(0);
	}
	
	public int getOccurrences(DataColumn column, Object value) {
		if(!this.existingValueOccurrences.containsKey(column))
			return -1;		// TODO	Or throw InvalidColumnException?
			
		if(!this.existingValueOccurrences.get(column).containsKey(value))
			return 0;
		
		return this.existingValueOccurrences.get(column).get(value);
	}
	
	public int getOccurrences(int columnIndex, Object value) {
		return getOccurrences(this.columns.get(columnIndex), value);
	}
	
	public Map<Object, Integer> getOccurrences(DataColumn column) {
		if(!this.existingValueOccurrences.containsKey(column))
			return null;		// TODO	Or throw InvalidColumnException?
		
		return this.existingValueOccurrences.get(column);
	}
	
	public Map<Object, Integer> getOccurrences(int columnIndex) {
		return existingValueOccurrences.get(this.columns.get(columnIndex));
	}
	
	public DataColumn getClassColumn() {
		return this.columns.get(this.classIndex);
	}
	
	public int getClassIndex() {
		return this.classIndex;
	}
	
	public String getClassName() {
		return this.className;
	}
	
	public boolean isClass(DataColumn column) {
		return column == this.getClassColumn();
	}
	
	public void setClass(int classIndex) {
		if(classIndex >= this.columns.size())
			return;			// TODO Or throw invalid column exception?
		
		this.classIndex = classIndex;
		this.className = this.columnNames.get(classIndex);
	}
	
	public boolean isClassColumnAltered() {
		return classColumnAltered;
	}
	
	// package-private access
	protected void setClassColumnAltered(boolean classColumnAltered) {
		this.classColumnAltered = classColumnAltered;
	}
	
	public Object getClassValueOf(int rowIndex) {
		return this.rows.get(rowIndex).get(this.classIndex);
	}
	
	public void setClassValueOf(int rowIndex, Object value) {
		this.rows.get(rowIndex).set(this.classIndex, value);
		
		setClassColumnAltered(true);
	}
	
	public void set(int row, int columnIndex, Object newValue) {
		Object oldValue = rows.get(row).get(columnIndex);
		
		if(newValue.equals(oldValue))
			return;
		
		DataColumn column = this.columns.get(columnIndex);
		
		rows.get(row).set(columnIndex, newValue);
		
		this.decreaseOccurrences(column, oldValue);
		this.registerPossibleValueOccurrence(column, newValue);
	}
	
	public void removeColumn(DataColumn column) {
		int columnIndex = this.columns.indexOf(column);
		removeColumn(columnIndex);
	}
	
	public void removeColumn(String columnName) {
		int columnIndex = this.indexOf(columnName);
		removeColumn(columnIndex);
	}
	
	public void removeColumn(int columnIndex) {
		DataColumn columnToRemove = this.getColumn(columnIndex);
		
		this.columnNames.remove(columnIndex);
		this.columns.remove(columnIndex);
		
		for(DataRow row : rows)
			row.removeColumn(columnIndex);
		
		//this.possibleValues.remove(columnToRemove);		// TODO keep in mind
		this.existingValues.remove(columnToRemove);
		this.existingValueOccurrences.remove(columnToRemove);
		
		// in case the are equal, class is being removed
		if(classIndex > columnIndex)
			classIndex--;
		else if(classIndex == columnIndex) {
			classIndex--;
			setClassColumnAltered(true);
			
			if(this.columns.size() > 0)
				className = this.columnNames.get(classIndex);
			else
				className = null;
		}
	}
	
	public int indexOf(DataColumn column) {
		return columns.indexOf(column);
	}
	
	public int indexOf(String columnName) {
		return columnNames.indexOf(columnName);
	}
	
	public boolean isEmpty() {
		return this.rowCount() == 0;
	}
	
	public int rowCount() {
		return this.rows.size();
	}
	
	public int columnCount() {
		return this.columns.size();
	}
	
	public Map<Object, DataTable> splitAt(DataColumn column) {
		Map<Object, DataTable> result = new HashMap<>();
		
		for(Object value : existingValues.get(column))
			result.put(value, new DataTable());
		
		Object currentValue;
		// iterate the current DataTable's rows and put each one to the corresponding sub-table
		for(DataRow row : this.rows) {
			currentValue = row.get(column);
			result.get(currentValue).addRow(new DataRow(row));
		}
		
		for(DataTable subTable : result.values())
			subTable.removeColumn(column);
		
		return result;
	}
	
	public Map<Object, DataTable> splitAt(int columnIndex) {
		DataColumn column = columns.get(columnIndex);
		return splitAt(column);
	}
	
	public Map<Object, DataTable> splitAt(String columnName) {
		int index = columnNames.indexOf(columnName);
		return splitAt(index);
	}
	
	@Deprecated
	public Map<Object, DataTable> splitAtInefficient(int columnIndex) {		// inefficient - keeping for testing purposes
		Map<Object, DataTable> result = new HashMap<>();
		
		DataColumn column = columns.get(columnIndex);
		
		for(Object value : existingValues.get(column))
			result.put(value, new DataTable(this, columnIndex, value));
		
		return result;
	}
	
	public DataRow createEmptyRow() {
		return DataRow.crateEmptyRow(this.columns);
	}
	
	public DataRow createDefaultRow() {
		return DataRow.createDefaultRow(this.columns);
	}
	
	public void addRow(DataRow row) {
		// if the DataTable's columns have not been defined yet (probably because it has no rows yet), use the given row to do so
		if(this.columnCount() == 0) {
			int i =0;
			boolean classFound = false;
			for(DataColumn column : row.getIndexedColumns()) {
				addColumn(column);
				
				if(!classFound && (column.getName().equalsIgnoreCase("class") || column.getName().equalsIgnoreCase("category"))) {
					this.classIndex = i;
					classFound = true;
				}
				i++;
			}
			
			// if the class has not been found, set the last column as the class column
			if(!classFound)
				this.classIndex = this.columnCount() - 1;
			this.className = this.columnNames.get(this.classIndex);
		}
		
		this.rows.add(new DataRow(row));
		
		for(DataColumn column : this.columns) {
			Object columnValue = row.get(column);
			registerPossibleValueOccurrence(column, columnValue);
		}
		
	}
	
	public void addColumn(DataColumn column) {
		columnNames.add(column.getName());
		columns.add(column);
		
		for(DataRow row : rows)
			row.addColumn(column);
		
		possibleValues.put(column, new TreeSet<>());
		existingValues.put(column, new TreeSet<>());
		existingValueOccurrences.put(column, new HashMap<>());
		
		if(column.isNumeric())
			possibleColumnBounds.put(column, new ArrayList<>());
	}
	
	public DataTable removeSubTable(int beginIndex) {
		return this.removeSubTable(beginIndex, this.rowCount() - 1);
	}
	
	public DataTable removeSubTable(int beginIndex, int endIndex) {
		if(beginIndex < 0 || beginIndex >= this.rowCount() ||
				endIndex < 0 || endIndex >= this.rowCount())
			throw new IndexOutOfBoundsException();
		
		DataTable subTable = new DataTable();
		
		for(int i = beginIndex; i < endIndex; i++)
			subTable.addRow(this.removeRow(beginIndex));
		
		return subTable;
	}
	
	public DataTable removeRandomSubTable(double percentage) {
		int subTableSize = (int) (this.rowCount() * percentage);
		
		if(subTableSize >= this.rowCount())
			return null;		// TODO or throw exception?
		
		DataTable subTable = new DataTable();
		
		int randomIndex;
		for(int i = 0; i < subTableSize; i++) {
			randomIndex = (int) (Math.random() * (this.rowCount() - 1));
			subTable.addRow(this.removeRow(randomIndex));
		}
		
		return subTable;
	}
	
	private void registerPossibleValueOccurrence(DataColumn column, Object columnValue) {
		// if the value is already among existing values, increase its count
		// if not, start counting
		if(this.existingValueOccurrences.get(column).containsKey(columnValue))
			increaseOccurrences(column, columnValue);
		else
			initiateOccurrences(column, columnValue);
		
		if(column == this.columns.get(classIndex))
			setClassColumnAltered(true);
	}
	
	// gets called the first time a value appears in a DataColumn
	private void initiateOccurrences(DataColumn column, Object columnValue) {
		this.possibleValues.get(column).add(columnValue);	// columnValue could already exist in here
		this.existingValues.get(column).add(columnValue);
		this.existingValueOccurrences.get(column).put(columnValue, 1);
		
		if(column.isNumeric()) {
			Double doubleValue = (Double) columnValue;
			List<Double> columnBounds = possibleColumnBounds.get(column);
			if(columnBounds.isEmpty()) {
				columnBounds.add(doubleValue);
				columnBounds.add(doubleValue);
			}
			else {
				if(doubleValue > columnBounds.get(1))
					columnBounds.set(1, doubleValue);
				else if (doubleValue < columnBounds.get(0))
					columnBounds.set(0, doubleValue);
			}
		}
	}
	
	private void increaseOccurrences(DataColumn column, Object columnValue) {
		Map<Object, Integer> valueOccurrences = this.existingValueOccurrences.get(column);
		valueOccurrences.put(columnValue, valueOccurrences.get(columnValue) + 1);
	}
	
	private void decreaseOccurrences(DataColumn column, Object columnValue) {
		Map<Object, Integer> valueOccurrences = this.existingValueOccurrences.get(column);
		
		int newOccurrenceCount = valueOccurrences.get(columnValue) - 1;
		
		// if the new occurrence count is zero, there is no need to consider the value a "possible" value anymore - hence instead of getting updated, it gets removed
		if(newOccurrenceCount > 0)
			valueOccurrences.put(columnValue, newOccurrenceCount);
		else {
			valueOccurrences.remove(columnValue);
			existingValues.get(column).remove(columnValue);		// also remove from existingValues
		}
		
		if(column == this.columns.get(classIndex))
			setClassColumnAltered(true);
	}
	
	public void print() {
		System.out.println(this.toString());
	}
	
	public void saveAs(String filepath) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(filepath));
		
		// the reason this method does not make use of DataTable.toString() is that toString() does not exactly follow the CSV format
		for(int i = 0; i < columnNames.size(); i++) {
			writer.append(columnNames.get(i));
			if(i != columnNames.size() - 1)
				writer.append(",");
		}
		
		writer.append("\n");
		
		for(DataRow dataRow : this.rows)
			writer.append(dataRow.toString()).append("\n");
		
		writer.close();
	}
	
	@Override
	public String toString() {
		if(this.isEmpty())
			return "";
		
		StringBuilder builder = new StringBuilder();
		
		for(int i = 0; i < columnNames.size(); i++) {
			builder.append(columnNames.get(i));
			if(i != columnNames.size() - 1)
				builder.append(", ");
		}
		
		builder.append("\n");
		
		for(DataRow dataRow : this.rows)
			builder.append(dataRow.toString()).append("\n");
		
		return builder.toString();
	}
	
}
