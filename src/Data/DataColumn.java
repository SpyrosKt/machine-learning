
package Data;

import java.util.Map;
import java.util.HashMap;

public class DataColumn {
	
	private String name;
	private Class type;
	public static final Class UNKNOWN_TYPE = Object.class;
	private Object defaultValue;
	private static Map<String, DataColumn> existingColumns = new HashMap<>();
	
	public DataColumn(String name, Class type) {
		this.name = name;
		this.type = type;
		
		if(this.isNumeric())
			defaultValue = 0.0;
		else if(isText())
			defaultValue = "-";
		else
			defaultValue = new Object();
		
		existingColumns.put(name, this);
	}
	
	public DataColumn(String name) {
		this.name = name;
		this.type = Object.class;
		
		existingColumns.put(name, this);
	}
	
	public String getName() {
		return name;
	}
	
	public Class getType() {
		return type;
	}
	
	public void setType(Class type) {
		this.type = type;
	}
	
	public Object getDefaultValue() {
		return defaultValue;
	}
	
	public void setDefaultValue(Object defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	public boolean isNumeric() {
		return this.type.getSuperclass() == Number.class;
	}
	
	private boolean isText() {
		return this.type == String.class;
	}
	
	@Override
	public String toString() {
		String result = name;
		result += " (";
		result += (isNumeric()) ? "" : "non-";
		result += "numeric)";
		
		return result;
	}
	
	protected static boolean columnExists(String columnName) {
		return existingColumns.containsKey(columnName);
	}
	
	public static DataColumn getColumn(String columnName) {
		return existingColumns.get(columnName);
	}
	
}
