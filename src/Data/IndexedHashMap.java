
package Data;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

public class IndexedHashMap<K, V> extends HashMap<K, V> implements Map<K, V> {
	
	protected List<K> indexedKeys = new ArrayList<>();
	
	
	public IndexedHashMap() {
		super();
	}
	
	public IndexedHashMap(Map<? extends K, ? extends V> source) {
		super(source);
		
		if(source instanceof IndexedHashMap) {
			IndexedHashMap castedMap = (IndexedHashMap) source;
			this.indexedKeys = new ArrayList<>(castedMap.indexedKeys);
		}
		else
			this.indexedKeys.addAll(source.keySet());
	}
	
	@Override
	public V put(K key, V value) {
		// if the key already exists in the map (and therefore in the ArrayList too), it shouldn't be added to the indexed keys
		if(!this.containsKey(key))
			indexedKeys.add(key);
		
		return super.put(key, value);
	}
	
	public V set(int index, V value) {
		K key = indexedKeys.get(index);		// this should perform any bound checks necessary
		return super.put(key, value);
	}
	
	@Override
	public V remove(Object key) {
		return this.remove(key, false);
	}
	
	public V remove(Object key, boolean indexedKeyAlreadyRemoved) {
		if(key instanceof Integer)
			this.remove(((Integer) key).intValue());
		
		V returnValue = super.remove(key);
		
		// a key was actually removed from the map and
		// the key has not already been removed from indexedKeys (by remove(int))
		if(returnValue != null && !indexedKeyAlreadyRemoved)
			indexedKeys.remove(key);
		
		return returnValue;
	}
	
	public V remove(int index) {
		K key = indexedKeys.get(index);
		indexedKeys.remove(index);
		return this.remove(key, true);
	}
	
	public K getKey(int index) {
		return indexedKeys.get(index);
	}
	
	public V get(int index) {
		K key = indexedKeys.get(index);
		return super.get(key);
	}
	
	public List<K> getIndexedKeys() {
		return indexedKeys;
	}
	
	public int indexOf(K key) {		// TODO Use object instead of K?
		return indexedKeys.indexOf(key);
	}
	
}
