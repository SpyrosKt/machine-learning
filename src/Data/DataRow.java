
package Data;

import java.util.List;

public class DataRow {
	
	protected IndexedHashMap<String, DataColumn> columns = new IndexedHashMap<>();
	protected IndexedHashMap<DataColumn, Object> columnValues = new IndexedHashMap<>();
	
	
	public DataRow(){}
	
	// copy constructor
	public DataRow(DataRow sourceDataRow) {
		this.columns = new IndexedHashMap<>(sourceDataRow.columns);
		this.columnValues = new IndexedHashMap<>(sourceDataRow.columnValues);
	}
	
	public static DataRow crateEmptyRow(List<DataColumn> columns) {
		DataRow row = new DataRow();
		
		for(DataColumn column : columns)
			row.columns.put(column.getName(), column);
		
		return row;
	}
	
	public static DataRow createDefaultRow(List<DataColumn> columns) {
		DataRow row = new DataRow();
		
		for(DataColumn column : columns) {
			row.columns.put(column.getName(), column);
			row.columnValues.put(column, column.getDefaultValue());
		}
		
		return row;
	}
	
	public Object get(int columnIndex) {return this.columnValues.get(columnIndex);}
	
	public Object get(String columnName) {
		return this.columnValues.get(columns.get(columnName));
	}
	
	public Object get(DataColumn column) {
		return this.columnValues.get(column);
	}
	
	public List<DataColumn> getIndexedColumns() {
		return this.columnValues.getIndexedKeys();
	}
	
	public void set(DataColumn column, Object value) {
		if(!this.columns.containsValue(column))
			return;		// TODO	Or throw InvalidColumnException?
		
		this.columnValues.put(column, value);
	}
	
	public void set(String columnName, Object value) {
		this.set(columns.get(columnName), value);
	}
	
	public void set(int columnIndex, Object value) {
		this.columnValues.set(columnIndex, value);
	}
	
	public void removeColumn(String columnName) {
		if(!columns.containsKey(columnName))
			return;		// TODO	Or throw InvalidColumnException?
		
		this.columnValues.remove(columns.get(columnName));
		this.columns.remove(columnName);
	}
	
	public void removeColumn(int columnIndex) {
		this.columnValues.remove(columnIndex);
		this.columns.remove(columnIndex);
	}
	
	public void addColumn(DataColumn column, Object value) {
		// don't add the column if it already exists
		if(columns.containsValue(column))
			return;
		
		columns.put(column.getName(), column);
		columnValues.put(column, value);
	}
	
	public void addColumn(DataColumn column) {
		addColumn(column, column.getDefaultValue());
	}
	
	public int size() {
		return this.columns.size();
	}
	
	public void print() {
		System.out.println(this.toStringMultiline());
	}
	
	public String toStringMultiline() {
		StringBuilder builder = new StringBuilder();
		
		for(int i = 0; i < this.size(); i++)
			builder.append(columns.get(i).getName()).append(": ").append(columnValues.get(i)).append("\n");
		
		return builder.toString();
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		for(int i = 0; i < this.size(); i++) {
			builder.append(columnValues.get(i));
			if(i < this.size() - 1)
				builder.append(", ");
		}
		
		return builder.toString();
	}
	
}
