
import Data.*;
import Bucketing.*;
import MachineLearning.*;

import java.io.IOException;

public class DummyMain {
	
	public static final int BUCKET_COUNT 		= 200;
	public static final int CLASS_BUCKET_COUNT 	= 200;
	
	public static void main(String[] args) throws IOException {
		
		DataTable trainData = new DataTable("C:\\Users\\Spyros\\Desktop\\inf131-2019\\train.csv");
		DataTable testData = new DataTable("C:\\Users\\Spyros\\Desktop\\inf131-2019\\test_with_cnt.csv");
		
		
		//Data.DataTable result = id3WithBucketing(trainData, testData);
		DataTable result = knn(trainData, testData);
		
		
		result.saveAs("C:\\Users\\Spyros\\Desktop\\tp.csv");
		
	}
	
	private static DataTable knn(DataTable trainData, DataTable testData) {
		KNN knn = new KNN(trainData, 4, KNN.DistanceCalculationMethod.EUCLIDIAN, KNN.NeighborCombinationMethod.MEDIAN);
		return knn.makePredictions(testData);
	}
	
	private static DataTable id3WithBucketing(DataTable trainData, DataTable testData) {
		BucketedDataTable bucketedTrainData = Bucketer.bucket(trainData, BUCKET_COUNT, CLASS_BUCKET_COUNT);
		BucketedDataTable bucketedTestData = Bucketer.bucket(testData, BUCKET_COUNT, CLASS_BUCKET_COUNT);
		BucketedDataTable bucketedValidationData = new BucketedDataTable(bucketedTrainData.removeRandomSubTable(0.3));
		
		DecisionTree bestTree = DecisionTree.randomForest(bucketedTrainData, bucketedValidationData, 30, 2.5);
		
		System.out.println("\nGot a best tree with threshold: " + bestTree.getThreshold());
		
		
		System.out.println("Making predictions...");
		
		DataTable predictions = bestTree.makePredictions(bucketedTestData);
		
		return Bucketer.debucket(predictions, bucketedTrainData.getBucketingParameters().get(bucketedTrainData.getClassColumn()), predictions.getColumn(1));
	}
	
}
