
package Bucketing;

import Data.*;

import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.HashSet;

public class Bucketer {
	
	public static final int DEFAULT_BUCKET_COUNT = 10;
	private static final int DEFAULT_CLASS_BUCKET_COUNT = 6;
	
	
	public static List<Double> getBounds(Set<Object> possibleValues) {
		Set<Double> castedValues = new HashSet<>();
		
		double lower = Double.MAX_VALUE;
		double upper = Double.MIN_VALUE;
		
		Double castedValue;
		for(Object value : possibleValues) {
			castedValue = (Double) value;
			
			if(castedValue < lower)
				lower = castedValue;
			
			if(castedValue > upper)
				upper = castedValue;
			
		}
		
		List<Double> result = new ArrayList<>();
		result.add(lower);
		result.add(upper);
		return result;
	}
	
	public static BucketedDataTable bucket(DataTable dataTable) {
		return bucket(dataTable, DEFAULT_BUCKET_COUNT, DEFAULT_CLASS_BUCKET_COUNT);
	}
	
	// TODO implement using bucket(Data.DataTable, Bucketing.BucketingParameters)
	public static BucketedDataTable bucket(DataTable dataTable, int bucketCount, int classBucketCount) {
		
		BucketedDataTable bucketedDataTable = new BucketedDataTable(dataTable);
		
		int columnCount = bucketedDataTable.columnCount();
		for(int columnIndex = 0; columnIndex < columnCount; columnIndex++) {
			
			DataColumn currentColumn = bucketedDataTable.getColumn(columnIndex);
			
			if(!currentColumn.isNumeric())
				continue;
			
			Set<Object> currentPossibleValues = bucketedDataTable.getPossibleValues(columnIndex);
			List<Double> currentBounds = getBounds(currentPossibleValues);
			
			double lower = currentBounds.get(0);
			double upper = currentBounds.get(1);
			double bucketSize;
			
			if(columnIndex != bucketedDataTable.getClassIndex())
				bucketSize = (upper - lower) / bucketCount;
			else
				bucketSize = (upper - lower) / classBucketCount;
			
			double castedValue;
			double diff;
			int currentRow = 0;
			for(DataRow dataRow : bucketedDataTable.getRows()) {
				castedValue = (Double) dataRow.get(columnIndex);
				
				diff = castedValue - lower;
				
				int bucketNumber = (int) Math.ceil(diff / bucketSize);
				
				
				//dataRow.set(columnIndex, bucketNumber + "");
				bucketedDataTable.set(currentRow, columnIndex, (double) bucketNumber);
				
				currentRow++;
			}
			
			bucketedDataTable.addBucketSize(currentColumn, bucketSize);
			bucketedDataTable.addLowerBound(currentColumn, lower);
			bucketedDataTable.addUpperBound(currentColumn, upper);
			
		}
		
		return bucketedDataTable;
		
	}
	
	public static BucketedDataTable bucket(DataTable dataTable, BucketingParameters parameters) {
		BucketedDataTable bucketedDataTable = new BucketedDataTable(dataTable);
		
		double lowerBound;
		double bucketSize;
		int bucketNumber;
		double castedValue;
		for(DataColumn column : bucketedDataTable.getColumns()) {
			if(!column.isNumeric())
				continue;
			
			lowerBound = parameters.get(column).getLowerBound();
			bucketSize = parameters.get(column).getBucketSize();
			
			for(DataRow row : bucketedDataTable.getRows()) {
				castedValue = (Double) row.get(column);
				bucketNumber = (int) Math.ceil((castedValue - lowerBound) / bucketSize);
				row.set(column, bucketNumber);
			}
		}
		
		return bucketedDataTable;
	}
	
	public static DataTable debucket(DataTable bucketedDataTable, BucketingParameters parameters) {
		DataTable debucketedDataTable = new DataTable(bucketedDataTable);
		
		double lowerBound;
		double bucketSize;
		for(DataColumn column : debucketedDataTable.getColumns()) {
			if(!column.isNumeric())
				continue;
			
			lowerBound = parameters.get(column).getLowerBound();
			bucketSize = parameters.get(column).getBucketSize();
			
			double approximateValue;	// an approximate value will be calculated given the bucketSize and lowerBound
			for(DataRow row : debucketedDataTable.getRows()) {
				try {
					approximateValue = lowerBound + ((Double) row.get(column) - 0.5) * bucketSize;
				}
				catch(Exception e) {
					approximateValue=0;
				}
				row.set(column, approximateValue);
			}
			
		}
		
		return debucketedDataTable;
	}
	
	public static DataTable debucket(DataTable bucketedDataTable, ColumnBucketingParameters parameters, DataColumn column) {
		DataTable debucketedDataTable = new DataTable(bucketedDataTable);
		
		if(!column.isNumeric())
			return debucketedDataTable;
		
		double lowerBound = parameters.getLowerBound();
		double bucketSize = parameters.getBucketSize();
		
		double approximateValue;	// an approximate value will be calculated given the bucketSize and lowerBound
		for(DataRow row : debucketedDataTable.getRows()) {
			try {
				approximateValue = lowerBound + ((Double) row.get(column) - 0.5) * bucketSize;
			}
			catch(Exception e) {
				approximateValue=0;
			}
			row.set(column, approximateValue);
		}
		
		return debucketedDataTable;
	}
	
}
