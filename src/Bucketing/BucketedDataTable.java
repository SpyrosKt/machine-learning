
package Bucketing;

import Data.DataColumn;
import Data.DataTable;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;

public class BucketedDataTable extends DataTable {
	
	private BucketingParameters bucketingParameters = new BucketingParameters();
	
	
	public BucketedDataTable(DataTable dataTable) {
		super(dataTable);
		this.possibleValues = new HashMap<>(this.existingValues);		// TODO should possibleValues be dismissed like this?
		
		this.possibleColumnBounds = new HashMap<>();
		
		// find column bounds based on the new possibleValues
		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		double castedValue;
		for(DataColumn column : this.columns) {
			if(!column.isNumeric())
				continue;
			
			for(Object value : this.possibleValues.get(column)) {
				castedValue = (Double) value;
				
				if(castedValue > max)
					max = castedValue;
				
				if(castedValue < min)
					min = castedValue;
			}
			
			this.possibleColumnBounds.put(column, new ArrayList<>(Arrays.asList(min, max)));
		}
	}
	
	BucketedDataTable(BucketedDataTable bucketedDataTable) {
		super(bucketedDataTable);
		this.bucketingParameters = new BucketingParameters(bucketedDataTable.bucketingParameters);
	}
	
	protected DataTable debucket() {
		return Bucketer.debucket(this, this.bucketingParameters);
	}
	
	protected DataTable debucket(BucketingParameters customParameters) {
		return Bucketer.debucket(this, customParameters);
	}
	
	protected void addLowerBound(DataColumn column, double lowerBound) {
		if(!this.columns.contains(column))
			return;		// TODO	Or throw InvalidColumnException?
		
		this.bucketingParameters.setLowerBound(column, lowerBound);
	}
	
	protected void addUpperBound(DataColumn column, double upperBound) {
		if(!this.columns.contains(column))
			return;		// TODO	Or throw InvalidColumnException?
		
		this.bucketingParameters.setUpperBound(column, upperBound);
	}
	
	protected void addBucketSize(DataColumn column, double bucketSize) {
		if(!this.columns.contains(column))
			return;		// TODO	Or throw InvalidColumnException?
		
		this.bucketingParameters.setBucketSize(column, bucketSize);
	}
	
	public BucketingParameters getBucketingParameters() {
		return this.bucketingParameters;
	}
	
}
