
package Bucketing;

import Data.DataColumn;

import java.util.Map;
import java.util.HashMap;

public class BucketingParameters {
	
	private Map<DataColumn, ColumnBucketingParameters> parameterMap = new HashMap<>();
	
	
	public BucketingParameters() {}
	
	public BucketingParameters(BucketingParameters bucketingParameters) {
		this.parameterMap = new HashMap<>(bucketingParameters.parameterMap);
	}
	
	public ColumnBucketingParameters get(DataColumn column) {
		return this.parameterMap.get(column);
	}
	
	public void setLowerBound(DataColumn column, double lowerBound) {
		if(!parameterMap.containsKey(column))
			parameterMap.put(column, new ColumnBucketingParameters());
		
		parameterMap.get(column).setLowerBound(lowerBound);
	}
	
	public void setUpperBound(DataColumn column, double upperBound) {
		if(!parameterMap.containsKey(column))
			parameterMap.put(column, new ColumnBucketingParameters());
		
		parameterMap.get(column).setUpperBound(upperBound);
	}
	
	public void setBucketSize(DataColumn column, double bucketSize) {
		if(!parameterMap.containsKey(column))
			parameterMap.put(column, new ColumnBucketingParameters());
		
		parameterMap.get(column).setBucketSize(bucketSize);
	}
	
}
