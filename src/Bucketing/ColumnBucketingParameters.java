
package Bucketing;

public class ColumnBucketingParameters {
	
	private double lowerBound, upperBound, bucketSize;
	
	
	public ColumnBucketingParameters() {}
	
	public ColumnBucketingParameters(double lowerBound, double upperBound, double bucketSize) {
		this.lowerBound = lowerBound;
		this.upperBound = upperBound;
		this.bucketSize = bucketSize;
	}
	
	public double getLowerBound() {
		return lowerBound;
	}
	
	public void setLowerBound(double lowerBound) {
		this.lowerBound = lowerBound;
	}
	
	public double getUpperBound() {
		return upperBound;
	}
	
	public void setUpperBound(double upperBound) {
		this.upperBound = upperBound;
	}
	
	public double getBucketSize() {
		return bucketSize;
	}
	
	public void setBucketSize(double bucketSize) {
		this.bucketSize = bucketSize;
	}
	
}
