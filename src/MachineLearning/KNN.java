
package MachineLearning;

import Data.*;

import java.util.*;

public class KNN extends MachineLearningAlgorithm {
	
	public enum DistanceCalculationMethod {
		EUCLIDIAN, MANHATTAN, MATCHES
	}
	
	public enum NeighborCombinationMethod {
		AVERAGE, MEDIAN
	}
	
	private DataTable trainData;
	
	public final static int DEFAULT_K = 8;
	private int k;
	public static final DistanceCalculationMethod DEFAULT_CALCULATION_METHOD = DistanceCalculationMethod.EUCLIDIAN;
	private DistanceCalculationMethod distanceCalculationMethod;
	public static final NeighborCombinationMethod DEFAULT_COMBINATION_METHOD = NeighborCombinationMethod.AVERAGE;
	private NeighborCombinationMethod neighborCombinationMethod;
	private Map<DataColumn, Double> weights = new HashMap<>();
	
	
	public KNN(DataTable trainData) {
		this(trainData, DEFAULT_K, DEFAULT_CALCULATION_METHOD, DEFAULT_COMBINATION_METHOD, false);
	}
	
	public KNN(DataTable trainData, int k, DistanceCalculationMethod distanceCalculationMethod, NeighborCombinationMethod neighborCombinationMethod) {
		this(trainData, k, distanceCalculationMethod, neighborCombinationMethod, false);
	}
	
	public KNN(DataTable trainData, int k, DistanceCalculationMethod distanceCalculationMethod, NeighborCombinationMethod neighborCombinationMethod, boolean weighted) {
		this.trainData = trainData;
		this.k = k;
		this.distanceCalculationMethod = distanceCalculationMethod;
		this.neighborCombinationMethod = neighborCombinationMethod;
		
		for(DataColumn column : trainData.getColumns()) {
			if(weighted) {
				if(column.isNumeric())
					weights.put(column, 1 / trainData.getColumnRange(column));
				else
					weights.put(column, 1 / (double) trainData.getPossibleValues(column).size());
			}
			else
				weights.put(column, 1.0);
		}
	}
	
	public Object makePrediction(DataRow rowToPredict) {
		Map<Double, DataRow> minDistanceNeighbors = new HashMap<>();
		
		double currentDistance;
		for(DataRow currentRow : trainData.getRows()) {
			switch(distanceCalculationMethod) {
				case EUCLIDIAN:
					currentDistance = euclidianDistance(rowToPredict, currentRow);
					break;
				case MANHATTAN:
					currentDistance = manhattanDistance(rowToPredict, currentRow);
					break;
				default:
					currentDistance = matchDistance(rowToPredict, currentRow);
			}
			
			if(minDistanceNeighbors.size() < k)
				minDistanceNeighbors.put(currentDistance, currentRow);
			else {
				double max = -1;
				
				for(Double distance : minDistanceNeighbors.keySet()) {
					if(distance > max)
						max = distance;
				}
				
				if(currentDistance < max) {
					minDistanceNeighbors.remove(max);
					minDistanceNeighbors.put(currentDistance, currentRow);
				}
			}
		}
		
		if(this.neighborCombinationMethod == NeighborCombinationMethod.AVERAGE)
			return average(minDistanceNeighbors.values());
		else
			return median(minDistanceNeighbors.values());
	}
	
	private double manhattanDistance(DataRow rowToPredict, DataRow currentRow) {
		double distance = 0;
		
		for(DataColumn column : trainData.getColumns()) {
			if(column == trainData.getColumn(trainData.getClassIndex()))
				continue;
			
			if(column.isNumeric())
				distance += weights.get(column) * Math.abs((Double) rowToPredict.get(column) - (Double) currentRow.get(column));
			else {
				if(!rowToPredict.get(column).equals(currentRow.get(column)))
					distance += weights.get(column);
			}
		}
		return distance;
	}
	
	private double euclidianDistance(DataRow rowToPredict, DataRow currentRow) {
		double distanceSquareSum = 0;
		
		for(DataColumn column : trainData.getColumns()) {
			if(column == trainData.getColumn(trainData.getClassIndex()))
				continue;
			
			if(column.isNumeric())
				distanceSquareSum += Math.pow(weights.get(column) * ((Double) rowToPredict.get(column) - (Double) currentRow.get(column)), 2);
			else {
				if(!rowToPredict.get(column).equals(currentRow.get(column)))
					distanceSquareSum += Math.pow(weights.get(column), 2);
			}
		}
		return Math.sqrt(distanceSquareSum);
	}
	
	private int matchDistance(DataRow rowToPredict, DataRow currentRow) {
		int distance = 0;
		
		for(DataColumn column : trainData.getColumns()) {
			if(column == trainData.getColumn(trainData.getClassIndex()))
				continue;
			
			if(!rowToPredict.get(column).equals(currentRow.get(column)))
				distance++;
		}
		
		return distance;
	}
	
	private Double average(Collection<DataRow> rows) {
		Double sum = 0.0;
		
		for(DataRow row : rows) {
			sum += (Double) row.get(this.trainData.getClassIndex());
		}
		
		return (sum / (double) rows.size());
	}
	
	private Double median(Collection<DataRow> rows) {
		List<DataRow> list = new ArrayList<>(rows);
		
		int classIndex = this.trainData.getClassIndex();
		
		list.sort((a, b) -> (int) Math.ceil((Double) a.get(classIndex) - (Double) b.get(classIndex)));
		
		int medianIndex = (list.size() - 1) / 2;
		return (Double) list.get(medianIndex).get(classIndex);
	}
	
}
