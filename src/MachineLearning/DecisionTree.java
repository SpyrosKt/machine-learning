
package MachineLearning;

import Data.*;

import java.util.*;

public class DecisionTree extends MachineLearningAlgorithm {
	
	private Node root = new Node();
	private int nodeCount = 1;		// starts at 1 - root already exists
	private double threshold;
	private Map<Node, Object> mostCommonClassValues = new HashMap<>();		// this will be helpful when there is no better way to make a decision
	
	
	private class Node {
		
		private DataColumn splitAttribute;				// the attribute based on which the Data.DataTable is split on this node
		Map<Object, Node> children = new TreeMap<>();	// each child is mapped to the value of decisiveAttribute that appears in it
		Object decision = null;							// the predicted value - non-null only in leaves
		
		Node() {}
		
		Node(DataColumn splitAttribute) {
			this.splitAttribute = splitAttribute;
		}
		
		Node(DataTable dataTable) {
			int indexOfHighestIGAttribute = Utilities.indexOfHighestIGAttribute(dataTable);
			this.splitAttribute = dataTable.getColumn(indexOfHighestIGAttribute);
		}
		
		public void setSplitAttribute(DataColumn splitAttribute) {
			this.splitAttribute = splitAttribute;
		}
		
		public void setDecision(Object decision) {
			this.decision = decision;
		}
		
		// maybe return a copy?
		public Map<Object, Node> getChildren() {
			return children;
		}
		
		public void addChild(Object value, Node child) {
			children.put(value, child);
		}
		
		public boolean isDecisionNode() {
			return decision != null;
		}
		
		public DataColumn getSplitAttribute() {
			return splitAttribute;
		}
		
		public Object getDecision() {
			return decision;
		}
		
		@Override
		public String toString() {
			if(decision != null)
				return ("Decision: " + decision.toString());
			
			return ("Split: " + splitAttribute.getName() + "?");
		}
		
	}
	
	
	/**
	 * Instead of expanding fully the tree, this constructor stops whenever the class' entropy (or the maximum IG?) reaches the threshold
	 * In such cases, the decision is taken according to the most common value in class
	 */
	DecisionTree(DataTable trainData, double threshold) {
		this.threshold = threshold;
		train(trainData);
	}
	
	DecisionTree(DataTable trainData) {
		this(trainData, 0);
	}
	
	private void train(DataTable trainData) {
		//Stack<Node> traversalStructure = new Stack<Node>();
		Queue<Node> traversalStructure = new LinkedList<>();
		Map<Node, DataTable> map = new HashMap<>();
		traversalStructure.add(root);
		map.put(root, trainData);
		
		Node currentNode;
		DataTable currentDataTable;
		Map<Object, DataTable> dataTableParts;
		while(!traversalStructure.isEmpty()) {
			
			currentNode = traversalStructure.remove();
			//currentNode = traversalStructure.pop();
			
			currentDataTable = map.get(currentNode);
			map.remove(currentNode);
			
			// in case the only attribute left is the class or if the class' entropy is low enough (judging using threshold)
			// then instead of expanding the tree, the current node is set to be a decision node
			if(currentDataTable.columnCount() == 1 || Utilities.calculateClassEntropy(currentDataTable) <= threshold) {
				currentNode.setDecision(Utilities.mostCommonClassValue(currentDataTable));
			}
			else {
				mostCommonClassValues.put(currentNode, Utilities.mostCommonClassValue(currentDataTable));
				
				currentNode.setSplitAttribute(Utilities.highestIGAttribute(currentDataTable));
				dataTableParts = currentDataTable.splitAt(currentNode.splitAttribute);
				
				for(Object key : dataTableParts.keySet()) {
					Node child = new Node();
					increaseNodeCount();
					
					currentNode.addChild(key, child);
					
					traversalStructure.add(child);
					map.put(child, dataTableParts.get(key));
				}
			}
			
		}
		
		System.out.println(nodeCount + " nodes created in total");
	}
	
	public static DecisionTree forest(DataTable trainData, DataTable validationData, List<Double> thresholds) {
		if(thresholds.isEmpty() || validationData.isEmpty())
			return new DecisionTree(trainData);
		
		DecisionTree bestTree = new DecisionTree(trainData);
		DecisionTree leastDeviantTree = bestTree;
		
		double maxScore = bestTree.testAccuracy(validationData);
		double minDeviation = leastDeviantTree.testDeviation(validationData);
		
		DecisionTree currentTree;
		double currentScore, currentDeviation;
		for(Double threshold : thresholds) {
			currentTree = new DecisionTree(trainData, threshold);
			currentScore = currentTree.testAccuracy(validationData);
			currentDeviation = currentTree.testDeviation(validationData);
			
			if(currentScore > maxScore) {
				maxScore = currentScore;
				bestTree = currentTree;
			}
			
			if(currentDeviation < minDeviation) {
				minDeviation = currentDeviation;
				leastDeviantTree = currentTree;
			}
		}
		
		return leastDeviantTree;
	}
	
	public static DecisionTree randomForest(DataTable trainData, DataTable validationData, int amountOfTrees) {
		return randomForest(trainData, validationData, amountOfTrees, 2);
	}
	
	public static DecisionTree randomForest(DataTable trainData, DataTable validationData, int amountOfTrees, double maxThreshold) {
		List<Double> randomizedThresholds = new ArrayList<>();
		
		randomizedThresholds.add(0.0);
		randomizedThresholds.add(maxThreshold);
		
		for(int i = 0; i < amountOfTrees - 2; i++)
			randomizedThresholds.add(Math.random() * maxThreshold);
		
		return forest(trainData, validationData, randomizedThresholds);
	}
	
	public double getThreshold() {
		return this.threshold;
	}
	
	// Predicts the class of a Data.DataRow
	protected Object makePrediction(DataRow row) {
		Node previousNode;
		Node currentNode = root;
		
		Object decisiveAttributeValue;
		while(!currentNode.isDecisionNode()) {
			decisiveAttributeValue = row.get(currentNode.getSplitAttribute());
			
			previousNode = currentNode;
			currentNode = currentNode.getChildren().get(decisiveAttributeValue);
			
			// if no child exists for decisiveAttributeValue, try and make a decision based on what was the most common value on the previous node
			if(currentNode == null)
				return mostCommonClassValues.get(previousNode);
			
		}
		
		return currentNode.decision;
	}
	
	private void increaseNodeCount() {
		nodeCount++;
		
		if(nodeCount % 5000 == 0)
			printNodeCount();
	}
	
	private void printNodeCount() {
		printTimeStamp();
		System.out.println(nodeCount + " nodes");
	}
	
}
