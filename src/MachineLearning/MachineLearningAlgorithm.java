
package MachineLearning;

import Data.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public abstract class MachineLearningAlgorithm {
	
	protected abstract Object makePrediction(DataRow row);
	
	// Tests the tree with the given datatable and returns the precision ratio of the decision tree
	public double testAccuracy(DataTable testData) {
		int correctDecisions = 0;
		
		for(int i = 0; i < testData.rowCount(); i++)
			if(testData.getClassValueOf(i).equals(makePrediction(testData.getRow(i))))
				correctDecisions++;
		
		return ((double) correctDecisions / testData.rowCount());
	}
	
	// can be used only if class is numeric
	public double testDeviation(DataTable testData) {
		double deviationSum = 0;
		
		for(int i = 0; i < testData.rowCount(); i++)
			deviationSum += Math.abs((Double) testData.getClassValueOf(i) - (Double) makePrediction(testData.getRow(i)));
		
		return deviationSum;
	}
	
	public DataTable makePredictions(DataTable testData) {
		double outputPercentage = 0.1;
		
		DataTable predictions = new DataTable();
		
		predictions.addColumn(new DataColumn("Id", String.class));
		predictions.addColumn(new DataColumn("Label", Double.class));
		predictions.setClass(1);
		
		DataRow currentRow;
		for(int i = 0; i < testData.rowCount(); i++) {
			currentRow = predictions.createEmptyRow();
			currentRow.set("Id", Integer.toString(i));
			currentRow.set("Label", makePrediction(testData.getRow(i)));
			
			predictions.addRow(currentRow);
			
			if((i + 1) % (int) (testData.rowCount() * outputPercentage) == 0) {
				printTimeStamp();
				System.out.println("Made prediction for row " + (i + 1) + "/" + testData.rowCount());
			}
		}
		
		return predictions;
	}
	
	protected static void printTimeStamp() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy  HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		System.out.print(dtf.format(now) + ":  ");
	}
	
}
